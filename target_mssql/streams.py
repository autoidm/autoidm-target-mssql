"""Stream class for target-mssql."""
from pathlib import Path
from typing import Any, Generator
from .singer_sdk.stream import Stream
from datetime import datetime
from dataclasses import dataclass
import time
import dateutil.parser
import logging
import pyodbc
import math
import base64
import uuid
import json
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")

#TODO: Use logging 
#TODO: Opening Database conneciton on a per stream basis seems like a no-go
class MSSQLStream(Stream):
  @dataclass
  class StreamToTableNames:
      stream_name: str
      schema_name: str
      table_uuid = uuid.uuid4()
      
      def temp_table_name(self) -> str:
          stream_name=self.stream_name.replace("-","_")
          return f"#{stream_name}"

      def schema_and_table_name(self) -> str:
          table_name = self.stream_name.replace("-","_")
          table_name = f"[{table_name}]"
          table_name = f"[{self.schema_name}]" + "." + table_name
          return table_name
      
      def schema_and_table_name_with_uuid(self) -> str:
          table_name = self.stream_name + str(self.table_uuid)
          table_name = table_name.replace("-","_")
          table_name = f"[{table_name}]"
          table_name = f"[{self.schema_name}]" + "." + table_name
          return table_name
      
      def table_name_with_uuid_old_appended(self) -> str:
          table_name = self.stream_name + str(self.table_uuid)
          table_name = table_name.replace("-","_")
          table_name = f"[{table_name}_old]"
          return table_name
      
      def schema_and_table_name_with_uuid_old_appended(self) -> str:
          table_name = self.stream_name + str(self.table_uuid)
          table_name = table_name.replace("-","_")
          table_name = f"[{table_name}_old]"
          table_name = f"[{self.schema_name}]" + "." + table_name
          return table_name

      def table_name(self) -> str:
          table_name = self.stream_name.replace("-","_")
          return table_name
      
      def table_name_with_uuid(self) -> str:
          table_name = self.stream_name + str(self.table_uuid)
          table_name = table_name.replace("-","_")
          return table_name

  """Stream class for MSSQL streams."""
  def __init__(self, conn, schema_name, batch_size, add_record_metadata, anyof_as_json, incremental_sync, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.incremental_sync = incremental_sync
    self.conn = conn
    self.conn.autocommit=True
    self.anyof_as_json = anyof_as_json
    #TODO Think about the right way to handle this when restructuring classes re https://pymssql.readthedocs.io/en/stable/pymssql_examples.html#important-note-about-cursors
    self.cursor = self.conn.cursor() 
    self.dml_sql = None
    self.stream_to_tablenames= self.StreamToTableNames(stream_name=self.name, schema_name=schema_name)
    self.batch_cache = [] 
    self.schema_name = schema_name
    self.batch_size = 10000 if batch_size is None else batch_size
    if add_record_metadata:
        self._add_sdc_metadata_to_schema()
    self.table_handler(self.stream_to_tablenames.temp_table_name())

  def _add_sdc_metadata_to_schema(self) -> None:
        """Add _sdc metadata columns.

        Record metadata specs documented at:
        https://sdk.meltano.com/en/latest/implementation/record_metadata.md
        """
        properties_dict = self.schema["properties"]
        for col in {
            "_sdc_extracted_at",
            "_sdc_received_at",
            "_sdc_batched_at",
            "_sdc_deleted_at",
        }:
            properties_dict[col] = {
                "type": ["null", "string"],
                "format": "date-time",
            }
        for col in {"_sdc_sequence", "_sdc_table_version"}:
            properties_dict[col] = {"type": ["null", "integer"]}

  def _add_sdc_metadata_to_record(
        self, record: dict, message: dict 
    ) -> None:
        """Populate metadata _sdc columns from incoming record message.

        Record metadata specs documented at:
        https://sdk.meltano.com/en/latest/implementation/record_metadata.md
        """
        record["_sdc_extracted_at"] = message.get("time_extracted")
        record["_sdc_received_at"] = datetime.now().isoformat()
        record["_sdc_batched_at"] = None #Not implemented yet 
        record["_sdc_deleted_at"] = None #Not implemented yet
        record["_sdc_sequence"] = int(round(time.time() * 1000))
        record["_sdc_table_version"] = None #Not implemented yet, message.get("version")

  def generate_full_table_name(self, streamname, schema_name):
    table_name = streamname
    table_name = streamname.replace("-","_")
    table_name = f"[{table_name}]"
    if schema_name: table_name = f"[{schema_name}]" + "." + table_name
    return table_name

  #TODO this method seems needless, should probably just call methods directly
  def table_handler(self, temp_table_name):
    #TODO it is not safe to assume you can truncate a table in this situation
    
    #TODO How do we know all of the data is through and we are ready to drop and merge data into the table?
    
    ddl = self.schema_to_temp_table_ddl(temp_table_name)
    self.ddl = ddl #Need access to column types when doing data processing( ie VARBINARY b64 decode)
    self.sql_runner(ddl)
  
  def schema_to_temp_table_ddl(self, table_name) -> str:
    self.name_type_mapping = {} #Created dict for Name to Value mapping, ultimeately for data conversion. TODO this is a bit clunky
    self.name_transform_mapping = {} #Created dict for Name to Value mapping, ultimeately for data conversion. TODO this is a bit clunky
    primary_key=None
    try:
      if(self.key_properties[0]):
          primary_key = self.key_properties[0]
    except AttributeError:
      primary_key=None

    properties=self.schema["properties"]
    
    sql = f"DROP TABLE IF EXISTS {table_name} CREATE TABLE {table_name}("
   
    #Key Properties
    #TODO can you assume only 1 primary key?
    if (primary_key):
      pk_type=self.ddl_json_to_mssqlmapping(self.schema["properties"][primary_key])
      #TODO Can't assume this is an INT always
      #TODO 450 is silly
      pk_type=pk_type.replace("MAX","450") #TODO hacky hacky
      sql += f"[{primary_key}] {pk_type} NOT NULL PRIMARY KEY"

    
    #Loop through all properties of stream to add them to our DDL
    for name, mssqltype in self._get_columns(skip_primary_key=True):
      sql+= f", [{name}] {mssqltype}"

    
    sql += ");"
    return sql
  
  def _get_columns(self, skip_primary_key: bool = False) -> Generator[tuple[str,str], None, None]:
      """Finds all columns and mssql types from the supplied schema.

      Args:
         skip_primary_key: Whether to skip the primary key in the yielded columns.
            Defaults to False.

      Raises:
         RuntimeError: If with_primary_key is False but no primary key is found that can
            be skipped.

     Yields:
         A tuple containing a column name and its mssql data type.
      """
      properties: dict[str, Any] = self.schema["properties"]

      primary_key = None
      try:
         if(self.key_properties[0]):
            primary_key = self.key_properties[0]
      except AttributeError as e:
         msg = "Primary key does not exist."
         raise RuntimeError(msg) from e

      if(skip_primary_key):
         properties.pop(primary_key, None)
      else:
         yield primary_key, None

      for name, shape in properties.items():
         mssqltype: str | None = self.ddl_json_to_mssqlmapping(shape)
         if (mssqltype is None): continue
         self.name_type_mapping.update({name:mssqltype})
         if ((self.anyof_as_json == True) and ("anyOf" in shape)):  self.name_transform_mapping.update({name:"json"})
         yield name, mssqltype
  
   #TODO what happens with multiple types
  def ddl_json_to_mssqlmapping(self, shape:dict) -> str:
    #TODO need to prioritize which type first
    #TODO anyOf Logic for transformation, and DDL should be located in the same place
    if ((self.anyof_as_json == True) and ("anyOf" in shape)): return "VARCHAR(MAX)"
    if ("type" not in shape): return None 
    jsontype = shape["type"]
    json_description = shape.get("description", None)
    json_max_length = shape.get("maxLength",None)
    json_format = shape.get("format", None)
    json_minimum = shape.get("minimum", None) 
    json_maximum = shape.get("maximum", None) 
    json_exclusive_minimum = shape.get("exclusiveMinimum", None)
    json_exclusive_maximum = shape.get("exclusiveMaximum", None)
    json_multiple_of = shape.get("multipleOf", None)
    mssqltype : str = None
    if ("string" in jsontype): 
        if(json_max_length and json_max_length < 8000 and json_description != "blob"): mssqltype = f"VARCHAR({json_max_length})" 
        elif(json_description == "blob"): mssqltype = f"VARBINARY(max)"
        elif(json_format == "date-time" and json_description == "date"): mssqltype = f"Date"
        elif(json_format == "date-time"): mssqltype = f"Datetime2(7)"
        else: mssqltype = "VARCHAR(MAX)"
    elif ("number" in jsontype): 
        if (json_minimum and json_maximum and json_exclusive_minimum and json_exclusive_maximum and json_multiple_of):
            #https://docs.microsoft.com/en-us/sql/t-sql/data-types/decimal-and-numeric-transact-sql?view=sql-server-ver15
            #p (Precision) Total number of decimal digits
            #s (Scale) Total number of decimal digits to the right of the decimal place
            
            max_digits_left_of_decimal = math.log10(json_maximum) 
            max_digits_right_of_decimal = -1*math.log10(json_multiple_of)
            percision : int = int(max_digits_left_of_decimal + max_digits_right_of_decimal)
            scale : int = int(max_digits_right_of_decimal)
            mssqltype = f"NUMERIC({percision},{scale})"
        else: 
            mssqltype = "NUMERIC(19,6)" 
    elif ("integer" in jsontype): mssqltype = "BIGINT" 
    elif ("boolean" in jsontype): mssqltype = "BIT"
     #not tested
    elif ("null" in jsontype): raise NotImplementedError("Can't set columns as null in MSSQL")
    elif ("array" in jsontype): raise NotImplementedError("Currently haven't implemented dealing with arrays")
    elif ("object" in jsontype): raise NotImplementedError("Currently haven't implemented dealing with objects")
    else: raise NotImplementedError(f"Haven't implemented dealing with this type of data. jsontype: {jsontype}") 
     
    return mssqltype

  def convert_data_to_params(self, datalist) -> list:
      parameters = []
      for noop in datalist:
          parameters.append("?")
      return parameters 
     
  #TODO when this is batched how do you make sure the column ordering stays the same (data class probs)
  #Columns is seperate due to data not necessairly having all of the correct columns
  def record_to_dml(self, table_name:str, data:dict) -> str:
    #TODO this is a bit gross, could refactor to make this easier to read
    column_list="],[".join(data.keys())
    sql = f"INSERT INTO {table_name} ([{column_list}])"

    paramaters = self.convert_data_to_params(data.values())
    sqlparameters = ",".join(paramaters)
    sql += f" VALUES ({sqlparameters})"
    return sql

  def sql_runner(self, sql):
    try:
      logging.info(f"Running SQL: {sql}")
      self.cursor.execute(sql)
    except Exception as e:
        logging.error(f"Caught exception whie running sql: {sql}")
        raise e

  def sql_runner_withparams(self, sql, paramaters):
    self.batch_cache.append(paramaters)
    if(len(self.batch_cache)>=self.batch_size):
      logging.info(f"Running batch with SQL: {sql} . Batch size: {len(self.batch_cache)}")
      self.commit_batched_data(sql, self.batch_cache)
      self.batch_cache = [] #Get our cache ready for more! 
  
  #This does not clear the cache that's up to you!
  def commit_batched_data(self, dml, cache):
    try:
      self.conn.autocommit = False
      self.cursor.fast_executemany = True 
      self.cursor.executemany(dml, cache)
    except pyodbc.DatabaseError as e:
      logging.error(f"Caught exception while running batch sql: {dml}. ")
      logging.debug(f"Caught exception while running batch sql: {dml}. Parameters for batch: {cache} ")
      self.conn.rollback()
      raise e
    else:
        self.conn.commit()
    finally:
        self.cursor.fast_executemany = False
        self.conn.autocommit = True #Set us back to the default of autoCommiting for other actions
        
  def data_conversion(self, name_ddltype_mapping, name_transform_mapping, record):
      newrecord = record
      if ("VARBINARY(max)" in name_ddltype_mapping.values() or "Date" in name_ddltype_mapping.values() or "Datetime2(7)" in name_ddltype_mapping.values() or len(name_transform_mapping) > 0): 
          for name, ddl in name_ddltype_mapping.items():
              if ddl=="VARBINARY(max)":
                  b64decode = None
                  if (record.get(name) is not None): b64decode = base64.b64decode(record.get(name))
                  #Tested this with the data that lands in the MSSQL database
                  #Take the hex data and convert them to bytes
                  #bytes = bytes.fromhex(hex) #remove hex indicator 0x from hex
                  #with open('file2.png', 'wb') as file
                  #  file.write(bytes)
                  #Example I used was a png, you'll need to determine type
                  record.update({name:b64decode})
              #https://gitlab.com/meltano/sdk/-/blob/main/singer_sdk/helpers/_typing.py#L179 looks to be a much better implementation, https://gitlab.com/autoidm/autoidm-target-mssql/-/issues/39 is in to migrate.
              if ddl=="Date":
                 date = record.get(name)
                 if (date is not None): 
                    transformed_date = dateutil.parser.isoparse(date)
                    newdate = transformed_date.strftime("%Y-%m-%d")
                    record.update({name:newdate})
              if ddl=="Datetime2(7)":
                 date = record.get(name)
                 if (date is not None): 
                     transformed_date = dateutil.parser.isoparse(date)
                     newdate = transformed_date.strftime("%Y-%m-%d %H:%M:%S.%f")
                     record.update({name:newdate})
              #AnyOf Data needed to be transformed to be a String instead of a list 
              if (name_transform_mapping.get(name) is not None): #This is a bit clunky
                  if (name_transform_mapping.get(name) == "json"):
                    transformed_data = json.dumps(record.get(name))
                    record.update({name:transformed_data})
                  else: raise Exception("Do not support other transforms at this time")

      return newrecord

  #Not actually persisting the record yet, batching
  def persist_record(self, record):
    dml = self.record_to_dml(table_name=self.stream_to_tablenames.temp_table_name(),data=record)    
    #TODO don't need to generate dml every time if we can be confident the ordering and data is correct (Singer forces this?)
    if (self.dml_sql is not None): 
        assert self.dml_sql == dml
    else: self.dml_sql = dml
    
    #Convert data
    record = self.data_conversion(self.name_type_mapping, self.name_transform_mapping, record)


    self.sql_runner_withparams(dml, tuple(record.values()))

  def clean_up(self):
      #Commit any batched records that are left
      if(len(self.batch_cache)>0):
          logging.info(f"Running batch with SQL: {self.dml_sql} . Batch size: {len(self.batch_cache)}")
          self.commit_batched_data(self.dml_sql, self.batch_cache)
      #We are good to go, need to be careful in case we have multiple processes trying to write to the same table at the same time in seperate processes
      try:
          self.conn.autocommit = False
          sql = f"DROP TABLE IF EXISTS {self.stream_to_tablenames.schema_and_table_name_with_uuid()}"
          logging.info(f"Running SQL: {sql}")
          self.cursor.execute(sql)
          #Moving to a table with a GUID first to make the time between dropping our full table, and moving the new one in as fast as possible
          sql = f"SELECT * INTO {self.stream_to_tablenames.schema_and_table_name_with_uuid()} from {self.stream_to_tablenames.temp_table_name()}"
          logging.info(f"Running SQL: {sql}")
          self.cursor.execute(sql)
          
      except pyodbc.DatabaseError as e:
          logging.error(f"Rolling back transaction. Caught exception while running SQL for {self.stream_to_tablenames.schema_and_table_name()} ")
          self.conn.rollback()
          raise e
      else:
          self.conn.commit()
      finally:
          self.conn.autocommit = True #Set us back to the default of autoCommiting for other actions
          logging.info(f"Table ready for the rename shuffle. TableName: {self.stream_to_tablenames.schema_and_table_name_with_uuid()}")
      
      if(not self.incremental_sync):
         #Cupid table shuffle, goal is to be sure we don't have any data overwriting eachother if there's a seperate process doing writes from the same stream at the same time
         try:
            self.conn.autocommit = False
            #Rename our temp table to the correct table
            sql = f"""
                  IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                  WHERE TABLE_SCHEMA = '{self.schema_name}'
                  AND  TABLE_NAME = '{self.stream_to_tablenames.table_name()}'))
                  BEGIN
                     EXEC sp_rename '{self.stream_to_tablenames.schema_and_table_name()}', {self.stream_to_tablenames.table_name_with_uuid_old_appended()} 
                  END
                  """
            logging.info(f"Running SQL: {sql}")
            self.cursor.execute(sql)
            
            sql = f"EXEC sp_rename '{self.stream_to_tablenames.schema_and_table_name_with_uuid()}', {self.stream_to_tablenames.table_name()}"  
            logging.info(f"Running SQL: {sql}")
            self.cursor.execute(sql)
         except pyodbc.DatabaseError as e:
            logging.error(f"Rolling back transaction. Caught exception while running SQL for {self.stream_to_tablenames.schema_and_table_name()} ")
            self.conn.rollback()
            raise e
         else:
            self.conn.commit()
         finally:
            self.conn.autocommit = True #Set us back to the default of autoCommiting for other actions 
         sql = f"DROP TABLE IF EXISTS {self.stream_to_tablenames.schema_and_table_name_with_uuid_old_appended()}"
         logging.info(f"Running SQL: {sql}")
         self.cursor.execute(sql)
         logging.info(f"Complete with table {self.stream_to_tablenames.schema_and_table_name()}")
      else:
         # Upserting records from UUID table to real table.
         try:
            self.conn.autocommit = False

            updates = []
            for name, _ in self._get_columns(skip_primary_key=True):
               updates.append(f"{name} = source.{name}")

            values = []
            for name, _ in self._get_columns(skip_primary_key=False):
               values.append(f"source.{name}")

            # A primary key is needed for incremental updating to be possible.
            primary_key = ""
            try:
               if(self.key_properties[0]):
                  primary_key = self.key_properties[0]
            except AttributeError as e:
               msg = "Primary key does not exist."
               raise RuntimeError(msg) from e

            # When a match is found between primary keys, that means that the record is
            # out of date and needs to be updated, hence UPDATE SET. When no match is 
            # found between primary keys, that means the record doesn't exist yet and
            # needs to be added, hence INSERT VALUES.
            sql = f"""
               MERGE {self.stream_to_tablenames.schema_and_table_name()} AS target  
               USING {self.stream_to_tablenames.schema_and_table_name_with_uuid()} AS source  
               ON target.{primary_key} = source.{primary_key}
               WHEN MATCHED THEN
                  UPDATE SET {', '.join(updates)}
               WHEN NOT MATCHED THEN
                  INSERT
                  VALUES({', '.join(values)});
               """
            logging.info(f"Running SQL: {sql}")
            self.cursor.execute(sql)
         
         except pyodbc.DatabaseError as e:
            logging.error(f"Rolling back transaction. Caught exception while running SQL for {self.stream_to_tablenames.schema_and_table_name()} ")
            self.conn.rollback()
            raise e
         else:
            self.conn.commit()
         finally:
            self.conn.autocommit = True #Set us back to the default of autoCommiting for other actions 
         sql = f"DROP TABLE IF EXISTS {self.stream_to_tablenames.schema_and_table_name_with_uuid()}"
         logging.info(f"Running SQL: {sql}")
         self.cursor.execute(sql)
         logging.info(f"Complete with table {self.stream_to_tablenames.schema_and_table_name()}")
